/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hilos;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LaboratorioU005_11
 */
public class HelloRunnable implements Runnable {
    String nombre="";
    
    public HelloRunnable(String _nombre){
        nombre = _nombre;
    }
    
    public void run() {
        
        for(int i=0; i<100; i++){
            try {
                Thread.sleep(1000);
                System.out.println("Hola soy el hilo "+this.nombre+" y estoy contando "+i+" veces");    
            } catch (InterruptedException ex) {
                Logger.getLogger(HelloRunnable.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }                
    }

    public static void main(String args[]) {
        (new Thread(new HelloRunnable("Pancho"))).start();
        (new Thread(new HelloRunnable("Pedro"))).start();
        (new Thread(new HelloRunnable("Ramira"))).start();
    }
}